package ru.renegatumsoul.hr.services
import org.openqa.selenium.Keys
import org.openqa.selenium.chrome.ChromeDriver
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.PropertySource
import org.springframework.stereotype.Service

@Service
@PropertySource("classpath:application.properties")
class VkParseService(
        @Value("\${vk.login}") private val login: String,
        @Value("\${vk.password}") private val password: String) {

    fun parseVk() {
        getOsType()
        val driver = ChromeDriver()
        driver.get("http://www.vk.com")
        driver.findElementById("index_email").sendKeys(login)
        driver.findElementById("index_pass").sendKeys(password)
        driver.findElementById("index_login_button").click()
        driver.get("https://vk.com/search/people")
        val search = driver.findElementById("search_query")
        search.sendKeys("Денис Den")
        search.sendKeys(Keys.RETURN)
        driver.findElementByXPath("//*[contains(text(), 'Работа')]").click()
        val job = driver.findElementById("company")
        job.sendKeys("Программист")
        search.sendKeys(Keys.RETURN)
    }

    fun getOsType() {
        print(System.getProperty("os.name"))
        println(System.getProperty("os.arch"))
        when(System.getProperty("os.name")) {
            "Mac OS X" -> System.setProperty("webdriver.chrome.driver", "drivers/chromedriver_macos")
            "Linux" -> if(System.getProperty("os.arch") == "amd64") System.setProperty("webdriver.chrome.driver", "drivers/chromedriver_linux_64") else System.setProperty("webdriver.chrome.driver", "drivers/chromedriver_linux_32")
        }
    }
}