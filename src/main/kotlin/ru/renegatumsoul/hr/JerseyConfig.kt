package ru.renegatumsoul.hr

import org.glassfish.jersey.server.ResourceConfig
import org.springframework.context.annotation.Configuration
import org.springframework.stereotype.Component

@Configuration
@Component
class JerseyConfig : ResourceConfig() {
    init { packages(JerseyConfig::class.java.`package`.name) }
}