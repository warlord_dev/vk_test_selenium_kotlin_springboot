package ru.renegatumsoul.hr

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class VKTestApplication

fun main(args: Array<String>) {
    SpringApplication.run(VKTestApplication::class.java, *args)
}
