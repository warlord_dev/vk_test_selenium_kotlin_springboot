package ru.renegatumsoul.hr.controllers

import org.springframework.stereotype.Component
import ru.renegatumsoul.hr.services.VkParseService
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.core.Response

@Component
@Path("")
class VkController(
        val vkService: VkParseService
) {
    @Path("/")
    @GET
    fun vkTest(): Response? {
        vkService.parseVk()
        return Response.ok("gj").build()
    }
}